package com.applydigital.exercise.controller.article;

import com.applydigital.exercise.dto.blacklist.BlacklistArticleDTO;
import com.applydigital.exercise.dto.entity.article.ArticleDTO;
import com.applydigital.exercise.dto.search.FindArticlesDTO;
import com.applydigital.exercise.service.article.ArticleService;
import com.applydigital.exercise.validator.blacklist.BlacklistArticleDTOValidator;
import com.applydigital.exercise.validator.search.FindArticlesDTOValidator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.applydigital.exercise.controller.article.RoutingArticleController.*;

@RestController
@RequestMapping(ROUTING_ARTICLE_CONTROLLER)
public class ArticleController {

    private final ArticleService articleService;
    private final FindArticlesDTOValidator findArticlesDTOValidator;
    private final BlacklistArticleDTOValidator blacklistArticleDTOValidator;

    public ArticleController(final ArticleService articleService,
                             final FindArticlesDTOValidator findArticlesDTOValidator,
                             final BlacklistArticleDTOValidator blacklistArticleDTOValidator) {
        this.articleService = articleService;
        this.findArticlesDTOValidator = findArticlesDTOValidator;
        this.blacklistArticleDTOValidator = blacklistArticleDTOValidator;
    }

    /*
     * Page of articles
     */
    @InitBinder("findArticlesDTO")
    public void setupFind(WebDataBinder binder) {
        binder.addValidators(findArticlesDTOValidator);
    }

    @GetMapping(value = ROUTING_ARTICLE_FIND)
    public ResponseEntity<Page<ArticleDTO>> find(@Valid FindArticlesDTO findArticlesDTO, Pageable pageable) {
        return new ResponseEntity<>(articleService.findArticles(findArticlesDTO, pageable), HttpStatus.OK);
    }


    /*
     * Delete and blacklist an article by objectId
     */
    @InitBinder("blacklistArticleDTO")
    public void setupBlacklist(WebDataBinder binder) {
        binder.addValidators(blacklistArticleDTOValidator);
    }

    @PostMapping(ROUTING_ARTICLE_BLACKLIST)
    public ResponseEntity<Void> deleteAndBlacklist(@Valid @RequestBody BlacklistArticleDTO blacklistArticleDTO) {
        articleService.blacklist(blacklistArticleDTO.getObjectId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
