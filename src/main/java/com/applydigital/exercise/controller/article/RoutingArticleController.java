package com.applydigital.exercise.controller.article;

public class RoutingArticleController {

    public final static String ROUTING_ARTICLE_CONTROLLER = "/article";

    public final static String ROUTING_ARTICLE_FIND = "";
    public final static String ROUTING_ARTICLE_BLACKLIST = "/blacklist";
}
