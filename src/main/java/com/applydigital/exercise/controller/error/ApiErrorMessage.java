package com.applydigital.exercise.controller.error;

public enum ApiErrorMessage {

    UNKNOWN_CODE("APPLY-DIGITAL-000", "Unknown error"),
    UNABLE_TO_PERSIST_ELEMENT("APPLY-DIGITAL-001", "Unable to persist element"),

    STRING_BLANK("APPLY-DIGITAL-002", "This field can not be null, empty or whitespaces"),
    STRING_LENGTH("APPLY-DIGITAL-003", "The length of this string has to be between %d and %d characters"),

    ARTICLE_ALREADY_BLACKLISTED("APPLY-DIGITAL-004", "This article has been already blacklisted"),
    ARTICLE_NOT_FOUND("APPLY-DIGITAL-005", "Article not found");

    private final String code;
    private final String message;

    ApiErrorMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
