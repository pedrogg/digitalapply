package com.applydigital.exercise.controller.error;

import com.applydigital.exercise.exception.ApiValidationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;

import static java.util.stream.Collectors.toList;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Handles @Valid failures
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(populateFieldErrors(ex.getBindingResult().getFieldErrors()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles failures when requests are sending files
     */
    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(populateFieldErrors(ex.getBindingResult().getFieldErrors()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles ApiValidationException exception
     */
    @ExceptionHandler(ApiValidationException.class)
    public ResponseEntity<Object> apiValidationException(final ApiValidationException ex) {
        return new ResponseEntity<>(populateFieldErrors(ex.getFieldErrors()), HttpStatus.BAD_REQUEST);
    }

    private ApiErrorsView populateFieldErrors(List<FieldError> errors) {
        List<ApiFieldError> apiFieldErrors = errors.stream()
                .map(fieldError -> new ApiFieldError(
                        fieldError.getField(),
                        asString(fieldError.getCodes()),
                        fieldError.getDefaultMessage()))
                .collect(toList());
        return new ApiErrorsView.Builder()
                .fieldErrors(apiFieldErrors)
                .build();
    }

    private String asString(String[] codes) {
        try {
            return codes[codes.length - 1];
        } catch (Exception e) {
            return ApiErrorMessage.UNKNOWN_CODE.getCode();
        }
    }

}
