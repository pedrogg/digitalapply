package com.applydigital.exercise.api.algolia.client;

import com.applydigital.exercise.api.algolia.dto.AlgoliaPage;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AlgoliaApiClient {

    String ROUTING_ARTICLES_BY_DATE = "/api/v1/search_by_date";

    @GET(ROUTING_ARTICLES_BY_DATE)
    Call<AlgoliaPage> findArticles(@Query("query") String query,
                                   @Query("page") Integer page);

}
