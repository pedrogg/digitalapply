package com.applydigital.exercise.api.algolia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AlgoliaPageExhaustive {

    private Boolean nbHits;
    private Boolean typo;

    public Boolean getNbHits() {
        return nbHits;
    }

    public void setNbHits(Boolean nbHits) {
        this.nbHits = nbHits;
    }

    public Boolean getTypo() {
        return typo;
    }

    public void setTypo(Boolean typo) {
        this.typo = typo;
    }
}
