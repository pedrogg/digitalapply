package com.applydigital.exercise.api.algolia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AlgoliaPageProcessingTimingRequest {

    private Long roundTrip;

    public Long getRoundTrip() {
        return roundTrip;
    }

    public void setRoundTrip(Long roundTrip) {
        this.roundTrip = roundTrip;
    }
}
