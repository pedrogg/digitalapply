package com.applydigital.exercise.api.algolia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AlgoliaPageProcessingTimingAfterFetchFormat {

    private Long highlighting;
    private Long total;

    public Long getHighlighting() {
        return highlighting;
    }

    public void setHighlighting(Long highlighting) {
        this.highlighting = highlighting;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
