package com.applydigital.exercise.api.algolia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AlgoliaPageProcessingTimingAfterFetch {

    private AlgoliaPageProcessingTimingAfterFetchFormat format;
    private Long total;

    public AlgoliaPageProcessingTimingAfterFetchFormat getFormat() {
        return format;
    }

    public void setFormat(AlgoliaPageProcessingTimingAfterFetchFormat format) {
        this.format = format;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
