package com.applydigital.exercise.api.algolia.facade;

import com.applydigital.exercise.api.algolia.dto.AlgoliaPage;

import java.util.Optional;

public interface AlgoliaApiFacade {

    Optional<AlgoliaPage> findArticles(String query,
                                       Integer page);

}
