package com.applydigital.exercise.api.algolia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AlgoliaPageHitHighlightFullyResult extends AlgoliaPageHitHighlightResult {

    private Boolean fullyHighlighted;

    public Boolean getFullyHighlighted() {
        return fullyHighlighted;
    }

    public void setFullyHighlighted(Boolean fullyHighlighted) {
        this.fullyHighlighted = fullyHighlighted;
    }
}
