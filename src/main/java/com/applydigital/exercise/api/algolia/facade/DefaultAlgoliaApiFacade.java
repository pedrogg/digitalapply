package com.applydigital.exercise.api.algolia.facade;

import com.applydigital.exercise.api.algolia.client.AlgoliaApiClient;
import com.applydigital.exercise.api.algolia.dto.AlgoliaPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.util.Optional;

@Service
public class DefaultAlgoliaApiFacade implements AlgoliaApiFacade {

    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultAlgoliaApiFacade.class);

    private final AlgoliaApiClient algoliaApiClient;

    public DefaultAlgoliaApiFacade(final AlgoliaApiClient algoliaApiClient) {
        this.algoliaApiClient = algoliaApiClient;
    }

    @Override
    public Optional<AlgoliaPage> findArticles(String query, Integer page) {
        try {
            final Response<AlgoliaPage> response = algoliaApiClient.findArticles(
                    query,
                    page).execute();
            if (response.isSuccessful()) {
                return Optional.ofNullable(response.body());
            }
            LOGGER.error("Error calling AlgoliaAPI: {} {}", response.code(), response.errorBody().string());
        } catch (Exception ex) {
            LOGGER.error("Exception calling AlgoliaAPI", ex);
        }
        return Optional.empty();
    }

}
