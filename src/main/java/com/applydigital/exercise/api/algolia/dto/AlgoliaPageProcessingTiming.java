package com.applydigital.exercise.api.algolia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AlgoliaPageProcessingTiming {

    private AlgoliaPageProcessingTimingAfterFetch afterFetch;
    private AlgoliaPageProcessingTimingFetch fetch;
    private AlgoliaPageProcessingTimingRequest request;
    private Long total;

    public AlgoliaPageProcessingTimingAfterFetch getAfterFetch() {
        return afterFetch;
    }

    public void setAfterFetch(AlgoliaPageProcessingTimingAfterFetch afterFetch) {
        this.afterFetch = afterFetch;
    }

    public AlgoliaPageProcessingTimingFetch getFetch() {
        return fetch;
    }

    public void setFetch(AlgoliaPageProcessingTimingFetch fetch) {
        this.fetch = fetch;
    }

    public AlgoliaPageProcessingTimingRequest getRequest() {
        return request;
    }

    public void setRequest(AlgoliaPageProcessingTimingRequest request) {
        this.request = request;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
