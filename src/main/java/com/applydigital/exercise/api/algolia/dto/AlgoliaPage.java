package com.applydigital.exercise.api.algolia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AlgoliaPage {

    private List<AlgoliaPageHit> hits = new ArrayList<>();
    private Integer nbHits;
    private Integer page;
    private Integer nbPages;
    private Integer hitsPerPage;
    private Boolean exhaustiveNbHits;
    private Boolean exhaustiveTypo;
    private AlgoliaPageExhaustive exhaustive;
    private String query;
    private String params;
    private Long processingTimeMS;
    private Long serverTimeMS;

    public List<AlgoliaPageHit> getHits() {
        return hits;
    }

    public void setHits(List<AlgoliaPageHit> hits) {
        this.hits = hits;
    }

    public Integer getNbHits() {
        return nbHits;
    }

    public void setNbHits(Integer nbHits) {
        this.nbHits = nbHits;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getNbPages() {
        return nbPages;
    }

    public void setNbPages(Integer nbPages) {
        this.nbPages = nbPages;
    }

    public Integer getHitsPerPage() {
        return hitsPerPage;
    }

    public void setHitsPerPage(Integer hitsPerPage) {
        this.hitsPerPage = hitsPerPage;
    }

    public Boolean getExhaustiveNbHits() {
        return exhaustiveNbHits;
    }

    public void setExhaustiveNbHits(Boolean exhaustiveNbHits) {
        this.exhaustiveNbHits = exhaustiveNbHits;
    }

    public Boolean getExhaustiveTypo() {
        return exhaustiveTypo;
    }

    public void setExhaustiveTypo(Boolean exhaustiveTypo) {
        this.exhaustiveTypo = exhaustiveTypo;
    }

    public AlgoliaPageExhaustive getExhaustive() {
        return exhaustive;
    }

    public void setExhaustive(AlgoliaPageExhaustive exhaustive) {
        this.exhaustive = exhaustive;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Long getProcessingTimeMS() {
        return processingTimeMS;
    }

    public void setProcessingTimeMS(Long processingTimeMS) {
        this.processingTimeMS = processingTimeMS;
    }

    public Long getServerTimeMS() {
        return serverTimeMS;
    }

    public void setServerTimeMS(Long serverTimeMS) {
        this.serverTimeMS = serverTimeMS;
    }

    public static final class Builder {
        private List<AlgoliaPageHit> hits = new ArrayList<>();
        private Integer nbHits;
        private Integer page;
        private Integer nbPages;
        private Integer hitsPerPage;
        private Boolean exhaustiveNbHits;
        private Boolean exhaustiveTypo;
        private AlgoliaPageExhaustive exhaustive;
        private String query;
        private String params;
        private Long processingTimeMS;
        private Long serverTimeMS;

        public Builder hits(List<AlgoliaPageHit> hits) {
            this.hits = hits;
            return this;
        }

        public Builder nbHits(Integer nbHits) {
            this.nbHits = nbHits;
            return this;
        }

        public Builder page(Integer page) {
            this.page = page;
            return this;
        }

        public Builder nbPages(Integer nbPages) {
            this.nbPages = nbPages;
            return this;
        }

        public Builder hitsPerPage(Integer hitsPerPage) {
            this.hitsPerPage = hitsPerPage;
            return this;
        }

        public Builder exhaustiveNbHits(Boolean exhaustiveNbHits) {
            this.exhaustiveNbHits = exhaustiveNbHits;
            return this;
        }

        public Builder exhaustiveTypo(Boolean exhaustiveTypo) {
            this.exhaustiveTypo = exhaustiveTypo;
            return this;
        }

        public Builder exhaustive(AlgoliaPageExhaustive exhaustive) {
            this.exhaustive = exhaustive;
            return this;
        }

        public Builder query(String query) {
            this.query = query;
            return this;
        }

        public Builder params(String params) {
            this.params = params;
            return this;
        }

        public Builder processingTimeMS(Long processingTimeMS) {
            this.processingTimeMS = processingTimeMS;
            return this;
        }

        public Builder serverTimeMS(Long serverTimeMS) {
            this.serverTimeMS = serverTimeMS;
            return this;
        }

        public AlgoliaPage build() {
            AlgoliaPage algoliaPage = new AlgoliaPage();
            algoliaPage.setHits(hits);
            algoliaPage.setNbHits(nbHits);
            algoliaPage.setPage(page);
            algoliaPage.setNbPages(nbPages);
            algoliaPage.setHitsPerPage(hitsPerPage);
            algoliaPage.setExhaustiveNbHits(exhaustiveNbHits);
            algoliaPage.setExhaustiveTypo(exhaustiveTypo);
            algoliaPage.setExhaustive(exhaustive);
            algoliaPage.setQuery(query);
            algoliaPage.setParams(params);
            algoliaPage.setProcessingTimeMS(processingTimeMS);
            algoliaPage.setServerTimeMS(serverTimeMS);
            return algoliaPage;
        }
    }
}
