package com.applydigital.exercise.api.algolia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AlgoliaPageHitHighlight {

    private AlgoliaPageHitHighlightResult author;
    private AlgoliaPageHitHighlightFullyResult comment_text;
    private AlgoliaPageHitHighlightFullyResult story_title;
    private AlgoliaPageHitHighlightFullyResult story_url;

    public AlgoliaPageHitHighlightResult getAuthor() {
        return author;
    }

    public void setAuthor(AlgoliaPageHitHighlightResult author) {
        this.author = author;
    }

    public AlgoliaPageHitHighlightFullyResult getComment_text() {
        return comment_text;
    }

    public void setComment_text(AlgoliaPageHitHighlightFullyResult comment_text) {
        this.comment_text = comment_text;
    }

    public AlgoliaPageHitHighlightFullyResult getStory_title() {
        return story_title;
    }

    public void setStory_title(AlgoliaPageHitHighlightFullyResult story_title) {
        this.story_title = story_title;
    }

    public AlgoliaPageHitHighlightFullyResult getStory_url() {
        return story_url;
    }

    public void setStory_url(AlgoliaPageHitHighlightFullyResult story_url) {
        this.story_url = story_url;
    }
}
