package com.applydigital.exercise.api.algolia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AlgoliaPageHit {

    private String created_at;
    private Long created_at_i;
    private String title;
    private String url;
    private String author;
    private Integer points;
    private String story_text;
    private String comment_text;
    private Integer num_comments;
    private Integer story_id;
    private String story_title;
    private String story_url;
    private Integer parent_id;
    private List<String> _tags = new ArrayList<>();
    private String objectID;
    private AlgoliaPageHitHighlight _highlightResult;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Long getCreated_at_i() {
        return created_at_i;
    }

    public void setCreated_at_i(Long created_at_i) {
        this.created_at_i = created_at_i;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getStory_text() {
        return story_text;
    }

    public void setStory_text(String story_text) {
        this.story_text = story_text;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public Integer getNum_comments() {
        return num_comments;
    }

    public void setNum_comments(Integer num_comments) {
        this.num_comments = num_comments;
    }

    public Integer getStory_id() {
        return story_id;
    }

    public void setStory_id(Integer story_id) {
        this.story_id = story_id;
    }

    public String getStory_title() {
        return story_title;
    }

    public void setStory_title(String story_title) {
        this.story_title = story_title;
    }

    public String getStory_url() {
        return story_url;
    }

    public void setStory_url(String story_url) {
        this.story_url = story_url;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    public List<String> get_tags() {
        return _tags;
    }

    public void set_tags(List<String> _tags) {
        this._tags = _tags;
    }

    public String getObjectID() {
        return objectID;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }

    public AlgoliaPageHitHighlight get_highlightResult() {
        return _highlightResult;
    }

    public void set_highlightResult(AlgoliaPageHitHighlight _highlightResult) {
        this._highlightResult = _highlightResult;
    }

    public static final class Builder {
        private String created_at;
        private Long created_at_i;
        private String title;
        private String url;
        private String author;
        private Integer points;
        private String story_text;
        private String comment_text;
        private Integer num_comments;
        private Integer story_id;
        private String story_title;
        private String story_url;
        private Integer parent_id;
        private List<String> _tags = new ArrayList<>();
        private String objectID;
        private AlgoliaPageHitHighlight _highlightResult;

        public Builder created_at(String created_at) {
            this.created_at = created_at;
            return this;
        }

        public Builder created_at_i(Long created_at_i) {
            this.created_at_i = created_at_i;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder points(Integer points) {
            this.points = points;
            return this;
        }

        public Builder story_text(String story_text) {
            this.story_text = story_text;
            return this;
        }

        public Builder comment_text(String comment_text) {
            this.comment_text = comment_text;
            return this;
        }

        public Builder num_comments(Integer num_comments) {
            this.num_comments = num_comments;
            return this;
        }

        public Builder story_id(Integer story_id) {
            this.story_id = story_id;
            return this;
        }

        public Builder story_title(String story_title) {
            this.story_title = story_title;
            return this;
        }

        public Builder story_url(String story_url) {
            this.story_url = story_url;
            return this;
        }

        public Builder parent_id(Integer parent_id) {
            this.parent_id = parent_id;
            return this;
        }

        public Builder _tags(List<String> _tags) {
            this._tags = _tags;
            return this;
        }

        public Builder objectID(String objectID) {
            this.objectID = objectID;
            return this;
        }

        public Builder _highlightResult(AlgoliaPageHitHighlight _highlightResult) {
            this._highlightResult = _highlightResult;
            return this;
        }

        public AlgoliaPageHit build() {
            AlgoliaPageHit algoliaPageHit = new AlgoliaPageHit();
            algoliaPageHit.setCreated_at(created_at);
            algoliaPageHit.setCreated_at_i(created_at_i);
            algoliaPageHit.setTitle(title);
            algoliaPageHit.setUrl(url);
            algoliaPageHit.setAuthor(author);
            algoliaPageHit.setPoints(points);
            algoliaPageHit.setStory_text(story_text);
            algoliaPageHit.setComment_text(comment_text);
            algoliaPageHit.setNum_comments(num_comments);
            algoliaPageHit.setStory_id(story_id);
            algoliaPageHit.setStory_title(story_title);
            algoliaPageHit.setStory_url(story_url);
            algoliaPageHit.setParent_id(parent_id);
            algoliaPageHit.set_tags(_tags);
            algoliaPageHit.setObjectID(objectID);
            algoliaPageHit.set_highlightResult(_highlightResult);
            return algoliaPageHit;
        }
    }
}
