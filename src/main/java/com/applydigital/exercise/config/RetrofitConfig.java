package com.applydigital.exercise.config;

import com.applydigital.exercise.api.algolia.client.AlgoliaApiClient;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.Objects;

@Configuration
public class RetrofitConfig {

    @Autowired
    private Environment environment;

    @Bean
    public AlgoliaApiClient algoliaApiClient() {
        return new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create())
                .baseUrl(Objects.requireNonNull(environment.getProperty("algolia.domain")))
                .client(new OkHttpClient.Builder().build())
                .build().create(AlgoliaApiClient.class);
    }

}
