package com.applydigital.exercise.config;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

import static com.applydigital.exercise.controller.article.RoutingArticleController.*;

@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .mvcMatchers(HttpMethod.POST, ROUTING_ARTICLE_CONTROLLER + ROUTING_ARTICLE_BLACKLIST).fullyAuthenticated()
                .mvcMatchers(HttpMethod.GET, ROUTING_ARTICLE_CONTROLLER + ROUTING_ARTICLE_FIND).fullyAuthenticated()
                .and().cors()
                .and().oauth2ResourceServer().jwt();
        return http.build();
    }

}
