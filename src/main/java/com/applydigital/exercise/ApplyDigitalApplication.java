package com.applydigital.exercise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
public class ApplyDigitalApplication {

    private final static Logger LOGGER = LoggerFactory.getLogger(ApplyDigitalApplication.class);

    public static void main(String[] args) {
        try {
            LOGGER.info("ApplyDigitalAPI running in " + InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
        }
        SpringApplication.run(ApplyDigitalApplication.class, args);
    }

}
