package com.applydigital.exercise.dto.entity.article;

import com.applydigital.exercise.dto.entity.common.AbstractDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ArticleDTO extends AbstractDTO {

    private String createdAt;

    private String objectId;
    private Integer parentId;
    private Integer storyId;

    private String title;
    private String url;
    private String author;
    private Integer points;

    private String storyText;
    private String storyTitle;
    private String storyUrl;

    private String commentText;
    private Integer numComments;

    private List<String> tags = new ArrayList<>();

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getStoryId() {
        return storyId;
    }

    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getStoryText() {
        return storyText;
    }

    public void setStoryText(String storyText) {
        this.storyText = storyText;
    }

    public String getStoryTitle() {
        return storyTitle;
    }

    public void setStoryTitle(String storyTitle) {
        this.storyTitle = storyTitle;
    }

    public String getStoryUrl() {
        return storyUrl;
    }

    public void setStoryUrl(String storyUrl) {
        this.storyUrl = storyUrl;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Integer getNumComments() {
        return numComments;
    }

    public void setNumComments(Integer numComments) {
        this.numComments = numComments;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public static final class Builder {
        private String createdAt;
        private String objectId;
        private Integer parentId;
        private Integer storyId;
        private String title;
        private String url;
        private String author;
        private Integer points;
        private String storyText;
        private String storyTitle;
        private String storyUrl;
        private String commentText;
        private Integer numComments;
        private List<String> tags = new ArrayList<>();
        private UUID id;

        public Builder createdAt(String createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder objectId(String objectId) {
            this.objectId = objectId;
            return this;
        }

        public Builder parentId(Integer parentId) {
            this.parentId = parentId;
            return this;
        }

        public Builder storyId(Integer storyId) {
            this.storyId = storyId;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder points(Integer points) {
            this.points = points;
            return this;
        }

        public Builder storyText(String storyText) {
            this.storyText = storyText;
            return this;
        }

        public Builder storyTitle(String storyTitle) {
            this.storyTitle = storyTitle;
            return this;
        }

        public Builder storyUrl(String storyUrl) {
            this.storyUrl = storyUrl;
            return this;
        }

        public Builder commentText(String commentText) {
            this.commentText = commentText;
            return this;
        }

        public Builder numComments(Integer numComments) {
            this.numComments = numComments;
            return this;
        }

        public Builder tags(List<String> tags) {
            this.tags = tags;
            return this;
        }

        public Builder id(UUID id) {
            this.id = id;
            return this;
        }

        public ArticleDTO build() {
            ArticleDTO articleDTO = new ArticleDTO();
            articleDTO.setCreatedAt(createdAt);
            articleDTO.setObjectId(objectId);
            articleDTO.setParentId(parentId);
            articleDTO.setStoryId(storyId);
            articleDTO.setTitle(title);
            articleDTO.setUrl(url);
            articleDTO.setAuthor(author);
            articleDTO.setPoints(points);
            articleDTO.setStoryText(storyText);
            articleDTO.setStoryTitle(storyTitle);
            articleDTO.setStoryUrl(storyUrl);
            articleDTO.setCommentText(commentText);
            articleDTO.setNumComments(numComments);
            articleDTO.setTags(tags);
            articleDTO.setId(id);
            return articleDTO;
        }
    }
}
