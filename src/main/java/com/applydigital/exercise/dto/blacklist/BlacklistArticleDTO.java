package com.applydigital.exercise.dto.blacklist;

public class BlacklistArticleDTO {

    private String objectId;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public static final class Builder {
        private String objectId;

        public Builder objectId(String objectId) {
            this.objectId = objectId;
            return this;
        }

        public BlacklistArticleDTO build() {
            BlacklistArticleDTO blacklistArticleDTO = new BlacklistArticleDTO();
            blacklistArticleDTO.setObjectId(objectId);
            return blacklistArticleDTO;
        }
    }
}
