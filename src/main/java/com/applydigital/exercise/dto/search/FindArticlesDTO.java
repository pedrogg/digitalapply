package com.applydigital.exercise.dto.search;

public class FindArticlesDTO {

    private String author;
    private String title;
    private String tag;
    private Months month;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Months getMonth() {
        return month;
    }

    public void setMonth(Months month) {
        this.month = month;
    }

    public static final class Builder {
        private String author;
        private String title;
        private String tag;
        private Months month;

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder tag(String tag) {
            this.tag = tag;
            return this;
        }

        public Builder month(Months month) {
            this.month = month;
            return this;
        }

        public FindArticlesDTO build() {
            FindArticlesDTO findArticlesDTO = new FindArticlesDTO();
            findArticlesDTO.setAuthor(author);
            findArticlesDTO.setTitle(title);
            findArticlesDTO.setTag(tag);
            findArticlesDTO.setMonth(month);
            return findArticlesDTO;
        }
    }
}
