package com.applydigital.exercise.converter.algolia;

import com.applydigital.exercise.api.algolia.dto.AlgoliaPageHit;
import com.applydigital.exercise.jpa.entity.article.ArticleEntity;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Objects;
import java.util.function.Function;

@Component
public class AlgoliaPageHitConverter implements Function<AlgoliaPageHit, ArticleEntity> {

    @Override
    public ArticleEntity apply(AlgoliaPageHit algoliaPageHit) {
        if (Objects.nonNull(algoliaPageHit)) {
            return new ArticleEntity.Builder()
                    .createdAt(Instant.parse(algoliaPageHit.getCreated_at()))
                    .objectId(algoliaPageHit.getObjectID())
                    .parentId(algoliaPageHit.getParent_id())
                    .storyId(algoliaPageHit.getStory_id())
                    .title(algoliaPageHit.getTitle())
                    .url(algoliaPageHit.getUrl())
                    .author(algoliaPageHit.getAuthor())
                    .points(algoliaPageHit.getPoints())
                    .storyText(algoliaPageHit.getStory_text())
                    .storyTitle(algoliaPageHit.getStory_title())
                    .storyUrl(algoliaPageHit.getStory_url())
                    .commentText(algoliaPageHit.getComment_text())
                    .numComments(algoliaPageHit.getNum_comments())
                    .build();
        }
        return null;
    }

}
