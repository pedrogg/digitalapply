package com.applydigital.exercise.converter.algolia;

import com.applydigital.exercise.jpa.entity.article.ArticleTagEntity;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.Function;

@Component
public class AlgoliaPageHitTagConverter implements Function<String, ArticleTagEntity> {

    @Override
    public ArticleTagEntity apply(String s) {
        if (Objects.nonNull(s)) {
            return new ArticleTagEntity.Builder()
                    .tag(s)
                    .build();
        }
        return null;
    }
}
