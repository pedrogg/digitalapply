package com.applydigital.exercise.converter.article;

import com.applydigital.exercise.jpa.entity.article.ArticleTagEntity;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.Function;

@Component
public class ArticleTagDTOConverter implements Function<ArticleTagEntity, String> {

    @Override
    public String apply(ArticleTagEntity articleTagEntity) {
        if (Objects.nonNull(articleTagEntity)) {
            return articleTagEntity.getTag();
        }
        return null;
    }

}
