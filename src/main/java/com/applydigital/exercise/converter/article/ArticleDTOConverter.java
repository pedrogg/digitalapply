package com.applydigital.exercise.converter.article;

import com.applydigital.exercise.dto.entity.article.ArticleDTO;
import com.applydigital.exercise.jpa.entity.article.ArticleEntity;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ArticleDTOConverter implements Function<ArticleEntity, ArticleDTO> {

    private final ArticleTagDTOConverter articleTagDTOConverter;

    public ArticleDTOConverter(final ArticleTagDTOConverter articleTagDTOConverter) {
        this.articleTagDTOConverter = articleTagDTOConverter;
    }

    @Override
    public ArticleDTO apply(ArticleEntity articleEntity) {
        if (Objects.nonNull(articleEntity)) {
            return new ArticleDTO.Builder()
                    .id(articleEntity.getId())
                    .createdAt(articleEntity.getCreatedAt().toString())
                    .objectId(articleEntity.getObjectId())
                    .parentId(articleEntity.getParentId())
                    .storyId(articleEntity.getStoryId())
                    .title(articleEntity.getTitle())
                    .url(articleEntity.getUrl())
                    .author(articleEntity.getAuthor())
                    .points(articleEntity.getPoints())
                    .storyText(articleEntity.getStoryText())
                    .storyTitle(articleEntity.getStoryTitle())
                    .storyUrl(articleEntity.getStoryUrl())
                    .commentText(articleEntity.getCommentText())
                    .numComments(articleEntity.getNumComments())
                    .tags(articleEntity.getTags().stream()
                            .map(articleTagDTOConverter)
                            .collect(Collectors.toList())
                    )
                    .build();
        }
        return null;
    }

}
