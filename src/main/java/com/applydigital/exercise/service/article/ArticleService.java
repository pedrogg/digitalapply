package com.applydigital.exercise.service.article;

import com.applydigital.exercise.dto.entity.article.ArticleDTO;
import com.applydigital.exercise.dto.search.FindArticlesDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ArticleService {

    Page<ArticleDTO> findArticles(FindArticlesDTO findArticlesDTO, Pageable pageable);

    void blacklist(String objectId);
}
