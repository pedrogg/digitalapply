package com.applydigital.exercise.service.article;

import com.applydigital.exercise.controller.error.ApiErrorMessage;
import com.applydigital.exercise.converter.article.ArticleDTOConverter;
import com.applydigital.exercise.dto.entity.article.ArticleDTO;
import com.applydigital.exercise.dto.search.FindArticlesDTO;
import com.applydigital.exercise.exception.ApiValidationException;
import com.applydigital.exercise.jpa.entity.article.ArticleEntity;
import com.applydigital.exercise.jpa.entity.blacklist.BlacklistArticleEntity;
import com.applydigital.exercise.jpa.repository.article.ArticleRepository;
import com.applydigital.exercise.jpa.repository.article.ArticleRepositorySpecification;
import com.applydigital.exercise.jpa.repository.blacklist.BlacklistArticleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DefaultArticleService implements ArticleService {

    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultArticleService.class);

    private final ArticleRepository articleRepository;
    private final BlacklistArticleRepository blacklistArticleRepository;
    private final ArticleDTOConverter articleDTOConverter;

    public DefaultArticleService(final ArticleRepository articleRepository,
                                 final BlacklistArticleRepository blacklistArticleRepository,
                                 final ArticleDTOConverter articleDTOConverter) {
        this.articleRepository = articleRepository;
        this.blacklistArticleRepository = blacklistArticleRepository;
        this.articleDTOConverter = articleDTOConverter;
    }

    @Override
    public Page<ArticleDTO> findArticles(FindArticlesDTO findArticlesDTO, Pageable pageable) {
        List<Specification<ArticleEntity>> specifications = new ArrayList<>();

        if (Objects.nonNull(findArticlesDTO.getAuthor())) {
            specifications.add(ArticleRepositorySpecification.authorLike(findArticlesDTO.getAuthor()));
        }

        if (Objects.nonNull(findArticlesDTO.getTitle())) {
            specifications.add(ArticleRepositorySpecification.titleLike(findArticlesDTO.getTitle()));
        }

        if (Objects.nonNull(findArticlesDTO.getTag())) {
            specifications.add(ArticleRepositorySpecification.tag(findArticlesDTO.getTag()));
        }

        if (Objects.nonNull(findArticlesDTO.getMonth())) {
            specifications.add(ArticleRepositorySpecification.month(findArticlesDTO.getMonth().getNumber()));
        }

        Specification<ArticleEntity> allFiltersApplied = specifications.stream()
                .reduce(Specification::and)
                .orElse(((root, query, criteriaBuilder) -> criteriaBuilder.conjunction()));

        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize());
        Page<ArticleEntity> page = articleRepository.findAll(allFiltersApplied, pageRequest);
        if (!page.getContent().isEmpty()) {
            return new PageImpl<>(
                    articleRepository.findByIds(page.getContent().stream().map(ArticleEntity::getId).collect(Collectors.toList()), pageable.getSort())
                            .stream()
                            .map(articleDTOConverter)
                            .collect(Collectors.toList()),
                    pageRequest,
                    page.getTotalElements()

            );
        }
        return new PageImpl<>(Collections.emptyList(), pageRequest, 0);
    }

    @Override
    public void blacklist(String objectId) {
        Optional<BlacklistArticleEntity> blacklistArticle = blacklistArticleRepository.findByObjectId(objectId);
        if (blacklistArticle.isPresent()) {
            throw new ApiValidationException("objectId", ApiErrorMessage.ARTICLE_ALREADY_BLACKLISTED.getCode(), ApiErrorMessage.ARTICLE_ALREADY_BLACKLISTED.getMessage());
        }

        ArticleEntity articleEntity = articleRepository.findByObjectId(objectId)
                .orElseThrow(() -> new ApiValidationException("objectId", ApiErrorMessage.ARTICLE_NOT_FOUND.getCode(), ApiErrorMessage.ARTICLE_NOT_FOUND.getMessage()));

        try {
            articleRepository.delete(articleEntity);
            blacklistArticleRepository.save(new BlacklistArticleEntity.Builder()
                    .objectId(objectId)
                    .build());
        } catch (Exception exception) {
            throw new ApiValidationException("article", ApiErrorMessage.UNABLE_TO_PERSIST_ELEMENT.getCode(), ApiErrorMessage.UNABLE_TO_PERSIST_ELEMENT.getMessage());
        }
    }

}
