package com.applydigital.exercise.service.feed;

import com.applydigital.exercise.api.algolia.dto.AlgoliaPage;
import com.applydigital.exercise.api.algolia.dto.AlgoliaPageHit;
import com.applydigital.exercise.api.algolia.facade.AlgoliaApiFacade;
import com.applydigital.exercise.converter.algolia.AlgoliaPageHitConverter;
import com.applydigital.exercise.converter.algolia.AlgoliaPageHitTagConverter;
import com.applydigital.exercise.jpa.entity.article.ArticleEntity;
import com.applydigital.exercise.jpa.repository.article.ArticleRepository;
import com.applydigital.exercise.jpa.repository.blacklist.BlacklistArticleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DefaultFeedService implements FeedService {

    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultFeedService.class);
    private final static String ALGOLIA_UNIQUE_CONSTRAINT_OBJECT_ID = "articleuniqueobjectid";

    private final AlgoliaApiFacade algoliaApiFacade;
    private final ArticleRepository articleRepository;
    private final BlacklistArticleRepository blacklistArticleRepository;
    private final AlgoliaPageHitConverter algoliaPageHitConverter;
    private final AlgoliaPageHitTagConverter algoliaPageHitTagConverter;

    public DefaultFeedService(final AlgoliaApiFacade algoliaApiFacade,
                              final ArticleRepository articleRepository,
                              final BlacklistArticleRepository blacklistArticleRepository,
                              final AlgoliaPageHitConverter algoliaPageHitConverter,
                              final AlgoliaPageHitTagConverter algoliaPageHitTagConverter) {
        this.algoliaApiFacade = algoliaApiFacade;
        this.articleRepository = articleRepository;
        this.blacklistArticleRepository = blacklistArticleRepository;
        this.algoliaPageHitConverter = algoliaPageHitConverter;
        this.algoliaPageHitTagConverter = algoliaPageHitTagConverter;
    }

    @Scheduled(fixedDelayString = "${algolia.articles.refresh-delay}")
    @Override
    public void populateArticles() {
        LOGGER.info("Starting to populate articles from Algolia");
        Instant start = Instant.now();

        int startPage = 0;
        int endPage = 50;
        List<AlgoliaPageHit> articlesToPersist = new ArrayList<>();

        // Retrieve all the articles
        while (startPage <= endPage) {
            Optional<AlgoliaPage> page = algoliaApiFacade.findArticles("java", startPage);
            if (page.isPresent()) {
                articlesToPersist.addAll(page.get().getHits().stream()
                        .filter(algoliaPageHit -> blacklistArticleRepository.findByObjectId(algoliaPageHit.getObjectID()).isEmpty())
                        .collect(Collectors.toList()));
                if (Objects.equals(startPage, 0)) {
                    // AlgoliaAPI provides a maximum of 50 pages, set this value with the first response of the API
                    endPage = page.get().getNbPages();
                }
            }
            startPage++;
        }

        // Persist in DB the articles retrieved
        int articlesSaved = 0;
        for (AlgoliaPageHit hit : articlesToPersist) {
            ArticleEntity articleEntity = algoliaPageHitConverter.apply(hit);
            try {
                // Persist article without tags
                ArticleEntity articleWithoutTags = articleRepository.save(articleEntity);
                // Add tags to article
                hit.get_tags().stream()
                        .map(algoliaPageHitTagConverter)
                        .forEach(articleWithoutTags::addTag);
                // Persist tags
                articleRepository.save(articleWithoutTags);
                // If we are here the article wasnt duplicated so it was persisted in DB
                articlesSaved++;
            } catch (DataIntegrityViolationException dataIntegrityViolationException) {
                if (dataIntegrityViolationException.getLocalizedMessage().contains(ALGOLIA_UNIQUE_CONSTRAINT_OBJECT_ID)) {
                    LOGGER.info("Article with objectId={} already persisted in DB", articleEntity.getObjectId());
                } else {
                    LOGGER.info("Unable to persist Article in DB", dataIntegrityViolationException);
                }
            }
        }

        LOGGER.info("{} algolia articles persisted in {} seconds", articlesSaved, Duration.between(start, Instant.now()).toSeconds());
    }
}
