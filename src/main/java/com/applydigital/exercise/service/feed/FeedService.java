package com.applydigital.exercise.service.feed;

public interface FeedService {

    void populateArticles();

}
