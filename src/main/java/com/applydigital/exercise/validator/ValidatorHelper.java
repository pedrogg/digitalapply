package com.applydigital.exercise.validator;

import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;

public interface ValidatorHelper {

    void rejectIfStringIsBlank(String field, String string, Errors errors);

    void rejectStringIfNotInLength(String field, String string, int min, int max, Errors errors);

}
