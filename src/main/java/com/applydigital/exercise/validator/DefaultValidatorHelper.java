package com.applydigital.exercise.validator;

import com.applydigital.exercise.controller.error.ApiErrorMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
public class DefaultValidatorHelper implements ValidatorHelper {

    @Override
    public void rejectIfStringIsBlank(String field, String string, Errors errors) {
        if (StringUtils.isBlank(string)) {
            errors.rejectValue(field, ApiErrorMessage.STRING_BLANK.getCode(), ApiErrorMessage.STRING_BLANK.getMessage());
        }
    }

    @Override
    public void rejectStringIfNotInLength(String field, String string, int min, int max, Errors errors) {
        rejectIfStringIsBlank(field, string, errors);
        if (!StringUtils.isBlank(string)) {
            if ((string.length() < min) || (string.length() > max)) {
                errors.rejectValue(field, ApiErrorMessage.STRING_LENGTH.getCode(), String.format(ApiErrorMessage.STRING_LENGTH.getMessage(), min, max));
            }
        }
    }

}
