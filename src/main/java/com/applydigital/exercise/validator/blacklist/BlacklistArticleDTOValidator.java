package com.applydigital.exercise.validator.blacklist;

import com.applydigital.exercise.dto.blacklist.BlacklistArticleDTO;
import com.applydigital.exercise.validator.ValidatorHelper;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class BlacklistArticleDTOValidator implements Validator {

    private final ValidatorHelper validatorHelper;
    private final Environment environment;

    public BlacklistArticleDTOValidator(final ValidatorHelper validatorHelper,
                                        final Environment environment) {
        this.validatorHelper = validatorHelper;
        this.environment = environment;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return BlacklistArticleDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BlacklistArticleDTO blacklistArticleDTO = (BlacklistArticleDTO) target;

        validatorHelper.rejectStringIfNotInLength(
                "objectId",
                blacklistArticleDTO.getObjectId(),
                Integer.parseInt(Objects.requireNonNull(environment.getProperty("objectId.min"))),
                Integer.parseInt(Objects.requireNonNull(environment.getProperty("objectId.max"))),
                errors);
    }

}
