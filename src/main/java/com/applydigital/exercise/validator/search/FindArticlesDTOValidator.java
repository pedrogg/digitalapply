package com.applydigital.exercise.validator.search;

import com.applydigital.exercise.dto.search.FindArticlesDTO;
import com.applydigital.exercise.validator.ValidatorHelper;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class FindArticlesDTOValidator implements Validator {

    private final ValidatorHelper validatorHelper;
    private final Environment environment;

    public FindArticlesDTOValidator(final ValidatorHelper validatorHelper,
                                    final Environment environment) {
        this.validatorHelper = validatorHelper;
        this.environment = environment;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return FindArticlesDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        FindArticlesDTO findArticlesDTO = (FindArticlesDTO) target;

        if (Objects.nonNull(findArticlesDTO.getAuthor())) {
            validatorHelper.rejectStringIfNotInLength(
                    "author",
                    findArticlesDTO.getAuthor(),
                    Integer.parseInt(Objects.requireNonNull(environment.getProperty("author.min"))),
                    Integer.parseInt(Objects.requireNonNull(environment.getProperty("author.max"))),
                    errors);
        }

        if (errors.hasErrors()) {
            return;
        }

        if (Objects.nonNull(findArticlesDTO.getTitle())) {
            validatorHelper.rejectStringIfNotInLength(
                    "title",
                    findArticlesDTO.getTitle(),
                    Integer.parseInt(Objects.requireNonNull(environment.getProperty("title.min"))),
                    Integer.parseInt(Objects.requireNonNull(environment.getProperty("title.max"))),
                    errors);
        }

        if (errors.hasErrors()) {
            return;
        }

        if (Objects.nonNull(findArticlesDTO.getTag())) {
            validatorHelper.rejectStringIfNotInLength(
                    "tag",
                    findArticlesDTO.getTag(),
                    Integer.parseInt(Objects.requireNonNull(environment.getProperty("tag.min"))),
                    Integer.parseInt(Objects.requireNonNull(environment.getProperty("tag.max"))),
                    errors);
        }
    }

}
