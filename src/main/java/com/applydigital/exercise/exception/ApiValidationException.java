package com.applydigital.exercise.exception;

import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

public class ApiValidationException extends RuntimeException {

    private final List<FieldError> fieldErrors = new ArrayList<>();
    private final String field;
    private final String code;
    private final String message;

    public ApiValidationException(String field, String code, String message) {
        this.field = field;
        this.code = code;
        this.message = message;
        fieldErrors.add(new FieldError(
                "object",
                field,
                null,
                false,
                new String[]{code},
                null,
                message
        ));
    }

    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }

    public String getField() {
        return field;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
