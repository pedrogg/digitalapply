package com.applydigital.exercise.jpa.entity.blacklist;

import com.applydigital.exercise.jpa.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "blacklist_article")
public class BlacklistArticleEntity extends AbstractEntity {

    private String objectId;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public static final class Builder {
        private UUID id;
        private String objectId;

        public Builder id(UUID id) {
            this.id = id;
            return this;
        }

        public Builder objectId(String objectId) {
            this.objectId = objectId;
            return this;
        }

        public BlacklistArticleEntity build() {
            BlacklistArticleEntity blacklistArticleEntity = new BlacklistArticleEntity();
            blacklistArticleEntity.setId(id);
            blacklistArticleEntity.setObjectId(objectId);
            return blacklistArticleEntity;
        }
    }
}
