package com.applydigital.exercise.jpa.entity.article;

import com.applydigital.exercise.jpa.entity.AbstractEntity;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.applydigital.exercise.jpa.repository.article.ArticleRepository.GRAPH_ARTICLE_FLAT;
import static com.applydigital.exercise.jpa.repository.article.ArticleRepository.GRAPH_ARTICLE_FULL;

@Entity
@Table(
        name = "article",
        uniqueConstraints = {@UniqueConstraint(name = "ArticleUniqueObjectId", columnNames = {"objectId"})})
@NamedEntityGraphs({
        @NamedEntityGraph(name = GRAPH_ARTICLE_FLAT),
        @NamedEntityGraph(
                name = GRAPH_ARTICLE_FULL,
                attributeNodes = {@NamedAttributeNode(value = "tags")}
        )
})
public class ArticleEntity extends AbstractEntity {

    private Instant createdAt;

    private String objectId;
    private Integer parentId;
    private Integer storyId;

    private String title;
    private String url;
    private String author;
    private Integer points;

    private String storyText;
    private String storyTitle;
    private String storyUrl;

    private String commentText;
    private Integer numComments;

    private List<ArticleTagEntity> tags = new ArrayList<>();

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getStoryId() {
        return storyId;
    }

    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getStoryText() {
        return storyText;
    }

    public void setStoryText(String storyText) {
        this.storyText = storyText;
    }

    public String getStoryTitle() {
        return storyTitle;
    }

    public void setStoryTitle(String storyTitle) {
        this.storyTitle = storyTitle;
    }

    public String getStoryUrl() {
        return storyUrl;
    }

    public void setStoryUrl(String storyUrl) {
        this.storyUrl = storyUrl;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Integer getNumComments() {
        return numComments;
    }

    public void setNumComments(Integer numComments) {
        this.numComments = numComments;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "article", fetch = FetchType.LAZY)
    public List<ArticleTagEntity> getTags() {
        return tags;
    }

    public void setTags(List<ArticleTagEntity> tags) {
        this.tags = tags;
    }

    public void addTag(ArticleTagEntity articleTagEntity) {
        articleTagEntity.setArticle(this);
        this.tags.add(articleTagEntity);
    }

    public void removeTag(ArticleTagEntity articleTagEntity) {
        articleTagEntity.setArticle(null);
        this.tags.remove(articleTagEntity);
    }

    public static final class Builder {
        private Instant createdAt;
        private String objectId;
        private Integer parentId;
        private Integer storyId;
        private String title;
        private String url;
        private String author;
        private Integer points;
        private String storyText;
        private String storyTitle;
        private String storyUrl;
        private String commentText;
        private Integer numComments;
        private List<ArticleTagEntity> tags = new ArrayList<>();
        private UUID id;

        public Builder createdAt(Instant createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder objectId(String objectId) {
            this.objectId = objectId;
            return this;
        }

        public Builder parentId(Integer parentId) {
            this.parentId = parentId;
            return this;
        }

        public Builder storyId(Integer storyId) {
            this.storyId = storyId;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder points(Integer points) {
            this.points = points;
            return this;
        }

        public Builder storyText(String storyText) {
            this.storyText = storyText;
            return this;
        }

        public Builder storyTitle(String storyTitle) {
            this.storyTitle = storyTitle;
            return this;
        }

        public Builder storyUrl(String storyUrl) {
            this.storyUrl = storyUrl;
            return this;
        }

        public Builder commentText(String commentText) {
            this.commentText = commentText;
            return this;
        }

        public Builder numComments(Integer numComments) {
            this.numComments = numComments;
            return this;
        }

        public Builder tags(List<ArticleTagEntity> tags) {
            this.tags = tags;
            return this;
        }

        public Builder id(UUID id) {
            this.id = id;
            return this;
        }

        public ArticleEntity build() {
            ArticleEntity articleEntity = new ArticleEntity();
            articleEntity.setCreatedAt(createdAt);
            articleEntity.setObjectId(objectId);
            articleEntity.setParentId(parentId);
            articleEntity.setStoryId(storyId);
            articleEntity.setTitle(title);
            articleEntity.setUrl(url);
            articleEntity.setAuthor(author);
            articleEntity.setPoints(points);
            articleEntity.setStoryText(storyText);
            articleEntity.setStoryTitle(storyTitle);
            articleEntity.setStoryUrl(storyUrl);
            articleEntity.setCommentText(commentText);
            articleEntity.setNumComments(numComments);
            articleEntity.setTags(tags);
            articleEntity.setId(id);
            return articleEntity;
        }
    }
}
