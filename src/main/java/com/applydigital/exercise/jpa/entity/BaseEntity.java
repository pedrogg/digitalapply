package com.applydigital.exercise.jpa.entity;

import java.util.UUID;

public interface BaseEntity {

    UUID getId();

    void setId(UUID id);

}
