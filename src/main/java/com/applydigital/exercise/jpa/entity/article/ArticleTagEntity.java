package com.applydigital.exercise.jpa.entity.article;

import com.applydigital.exercise.jpa.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "article_tag")
public class ArticleTagEntity extends AbstractEntity {

    private ArticleEntity article;
    private String tag;

    @ManyToOne(fetch = FetchType.LAZY)
    public ArticleEntity getArticle() {
        return article;
    }

    public void setArticle(ArticleEntity article) {
        this.article = article;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public static final class Builder {
        private ArticleEntity article;
        private String tag;
        private UUID id;

        public Builder article(ArticleEntity article) {
            this.article = article;
            return this;
        }

        public Builder tag(String tag) {
            this.tag = tag;
            return this;
        }

        public Builder id(UUID id) {
            this.id = id;
            return this;
        }

        public ArticleTagEntity build() {
            ArticleTagEntity articleTagEntity = new ArticleTagEntity();
            articleTagEntity.setArticle(article);
            articleTagEntity.setTag(tag);
            articleTagEntity.setId(id);
            return articleTagEntity;
        }
    }
}
