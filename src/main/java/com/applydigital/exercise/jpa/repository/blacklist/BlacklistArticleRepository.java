package com.applydigital.exercise.jpa.repository.blacklist;

import com.applydigital.exercise.jpa.entity.blacklist.BlacklistArticleEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.UUID;

public interface BlacklistArticleRepository extends PagingAndSortingRepository<BlacklistArticleEntity, UUID> {

    Optional<BlacklistArticleEntity> findByObjectId(String objectId);

}
