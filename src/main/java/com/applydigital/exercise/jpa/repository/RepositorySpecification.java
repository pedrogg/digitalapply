package com.applydigital.exercise.jpa.repository;

import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

public class RepositorySpecification {

    /**
     * Creates and returns a join on the root entity if that join was not already created, otherwise it returns
     * existing join.
     *
     * Composing multiple specifications that do the same joins on the root entity will unfortunately result with a
     * query with (unnecessary) multiple joins of the same entity. For this reason, we need to only do the join if that
     * particular join was not already done on the root entity.
     *
     * See more: https://stackoverflow.com/a/21823321/2092870
     */
    public static Join<?, ?> join(From<?, ?> from, String attribute) {
        for (Join<?, ?> join : from.getJoins()) {
            boolean sameName = join.getAttribute().getName().equals(attribute);
            if (sameName && join.getJoinType().equals(JoinType.LEFT)) {
                return join;
            }
        }
        return from.join(attribute, JoinType.LEFT);
    }

}
