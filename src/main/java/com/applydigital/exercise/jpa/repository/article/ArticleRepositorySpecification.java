package com.applydigital.exercise.jpa.repository.article;

import com.applydigital.exercise.jpa.entity.article.ArticleEntity;
import com.applydigital.exercise.jpa.entity.article.ArticleEntity_;
import com.applydigital.exercise.jpa.entity.article.ArticleTagEntity_;
import com.applydigital.exercise.jpa.repository.RepositorySpecification;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

public class ArticleRepositorySpecification extends RepositorySpecification {

    public static Specification<ArticleEntity> authorLike(String author) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder.upper(root.get(ArticleEntity_.AUTHOR)), "%" + author.toUpperCase() + "%");
    }

    public static Specification<ArticleEntity> titleLike(String title) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder.upper(root.get(ArticleEntity_.TITLE)), "%" + title.toUpperCase() + "%");
    }

    public static Specification<ArticleEntity> tag(String tag) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.upper(joinTags(root).get(ArticleTagEntity_.TAG)), tag.toUpperCase());
    }

    public static Specification<ArticleEntity> month(Integer month) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.function("MONTH", Integer.class, root.get(ArticleEntity_.CREATED_AT)), month);
    }

    private static Join<?, ?> joinTags(Root<ArticleEntity> root) {
        return join(root, ArticleEntity_.TAGS);
    }
}
