package com.applydigital.exercise.jpa.repository.article;

import com.applydigital.exercise.jpa.entity.article.ArticleEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hibernate.jpa.QueryHints.HINT_PASS_DISTINCT_THROUGH;

public interface ArticleRepository extends PagingAndSortingRepository<ArticleEntity, UUID>, JpaSpecificationExecutor<ArticleEntity> {

    String GRAPH_ARTICLE_FLAT = "article.flat";
    String GRAPH_ARTICLE_FULL = "article.full";

    @EntityGraph(value = GRAPH_ARTICLE_FLAT, type = EntityGraph.EntityGraphType.FETCH)
    Optional<ArticleEntity> findByObjectId(String objectId);

    @EntityGraph(value = GRAPH_ARTICLE_FLAT, type = EntityGraph.EntityGraphType.FETCH)
    Page<ArticleEntity> findAll(Specification<ArticleEntity> specification, Pageable pageable);

    @EntityGraph(value = GRAPH_ARTICLE_FULL, type = EntityGraph.EntityGraphType.FETCH)
    @QueryHints(value = @QueryHint(name = HINT_PASS_DISTINCT_THROUGH, value = "false"))
    @Query("SELECT DISTINCT articleEntity FROM ArticleEntity articleEntity WHERE articleEntity.id IN :ids")
    List<ArticleEntity> findByIds(@Param("ids") List<UUID> ids, Sort sort);

}
