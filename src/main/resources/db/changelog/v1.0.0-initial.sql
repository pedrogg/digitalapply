-------------------
-- ArticleEntity --
-------------------
CREATE TABLE article
(
    id           uuid NOT NULL,
    created_at   timestamp NULL,
    object_id    varchar(255) NULL,
    parent_id    varchar(255) NULL,
    story_id     varchar(255) NULL,

    title        varchar(255) NULL,
    url          varchar(1000) NULL,
    author       varchar(255) NULL,
    points       int4 NULL,

    story_text   varchar(6000) NULL,
    story_title  varchar(255) NULL,
    story_url    varchar(1000) NULL,

    comment_text varchar(10000) NULL,
    num_comments int4 NULL,
    CONSTRAINT article_pkey PRIMARY KEY (id),
    CONSTRAINT ArticleUniqueObjectId UNIQUE (object_id)
);

----------------------
-- ArticleTagEntity --
----------------------
CREATE TABLE article_tag
(
    id         uuid NOT NULL,
    article_id uuid NULL,
    tag        varchar(255) NULL,
    CONSTRAINT article_tag_pkey PRIMARY KEY (id),
    CONSTRAINT article_tag_article_fkey FOREIGN KEY (article_id) REFERENCES article (id)
);

----------------------------
-- BlacklistArticleEntity --
----------------------------
CREATE TABLE blacklist_article
(
    id        uuid NOT NULL,
    object_id varchar(255) NULL,
    CONSTRAINT blacklist_article_pkey PRIMARY KEY (id)
);