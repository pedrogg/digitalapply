-------------------
-- ArticleEntity --
-------------------
INSERT INTO article(id, created_at, object_id, parent_id, story_id, title, url, author, points, story_text, story_title, story_url, comment_text, num_comments)
VALUES('b0508288-1e97-4d50-9675-357be8c74499', '2023-03-16 18:39:44.000', '35186361', 35186765, 35186812, 'The Guide to Java 20 Changes', 'https://nipafx.dev/java-20-guide/', 'carimura', 11, NULL, NULL, NULL, NULL, 8);

----------------------
-- ArticleTagEntity --
----------------------
INSERT INTO article_tag(id, article_id, tag) VALUES('f1a6f70b-51c4-4108-9447-dc733989c325', 'b0508288-1e97-4d50-9675-357be8c74499', 'comment');
INSERT INTO article_tag(id, article_id, tag) VALUES('fba1c3f7-0b57-4ed7-951b-ecdfac20543e', 'b0508288-1e97-4d50-9675-357be8c74499', 'author_airstrike');
INSERT INTO article_tag(id, article_id, tag) VALUES('cb05c524-dd79-4393-a4ba-b1b3850ec3bd', 'b0508288-1e97-4d50-9675-357be8c74499', 'story_35186812');

----------------------------
-- BlacklistArticleEntity --
----------------------------
INSERT INTO blacklist_article(id, object_id) VALUES('9169cb5b-f2a3-4790-bc4a-b9c90078ff05', '35186361');