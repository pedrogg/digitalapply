package com.applydigital.exercise.jpa.repository.article

import org.hibernate.Hibernate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@SpringBootTest
@ActiveProfiles(value = "integrationTest")
class ArticleRepositorySpec extends Specification {

    @Autowired
    ArticleRepository articleRepository

    def "findByObjectId"() {
        when:
        final actual = articleRepository.findByObjectId("35186361")

        then:
        actual.isPresent()
    }

    def "ArticleRepositorySpecification.authorLike"() {
        given:
        final page = PageRequest.of(0, 20)

        when:
        final actual = articleRepository.findAll(ArticleRepositorySpecification.authorLike("CaRi"), page)

        then:
        actual.getContent().size() == 1

        when:
        final searchFullGraph = articleRepository.findByIds([actual.getContent().get(0).getId()], Sort.by(Sort.Order.desc("createdAt")))

        then:
        searchFullGraph.size() == 1

        and:
        searchFullGraph.each {
            assert Hibernate.isInitialized(it.getTags())
        }
    }

    def "ArticleRepositorySpecification.titleLike"() {
        given:
        final page = PageRequest.of(0, 20)

        when:
        final actual = articleRepository.findAll(ArticleRepositorySpecification.titleLike("JaVa"), page)

        then:
        actual.getContent().size() == 1

        when:
        final searchFullGraph = articleRepository.findByIds([actual.getContent().get(0).getId()], Sort.by(Sort.Order.desc("createdAt")))

        then:
        searchFullGraph.size() == 1

        and:
        searchFullGraph.each {
            assert Hibernate.isInitialized(it.getTags())
        }
    }

    def "ArticleRepositorySpecification.tag"() {
        given:
        final page = PageRequest.of(0, 20)

        when:
        final actual = articleRepository.findAll(ArticleRepositorySpecification.tag("author_airstrike"), page)

        then:
        actual.getContent().size() == 1

        when:
        final searchFullGraph = articleRepository.findByIds([actual.getContent().get(0).getId()], Sort.by(Sort.Order.desc("createdAt")))

        then:
        searchFullGraph.size() == 1

        and:
        searchFullGraph.each {
            assert Hibernate.isInitialized(it.getTags())
        }
    }

    def "ArticleRepositorySpecification.month"() {
        given:
        final page = PageRequest.of(0, 20)

        when:
        final actual = articleRepository.findAll(ArticleRepositorySpecification.month(3), page)

        then:
        actual.getContent().size() == 1

        when:
        final searchFullGraph = articleRepository.findByIds([actual.getContent().get(0).getId()], Sort.by(Sort.Order.desc("createdAt")))

        then:
        searchFullGraph.size() == 1

        and:
        searchFullGraph.each {
            assert Hibernate.isInitialized(it.getTags())
        }
    }

}
