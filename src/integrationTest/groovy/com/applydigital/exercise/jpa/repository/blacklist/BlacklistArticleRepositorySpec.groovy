package com.applydigital.exercise.jpa.repository.blacklist

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@SpringBootTest
@ActiveProfiles(value = "integrationTest")
class BlacklistArticleRepositorySpec extends Specification {

    @Autowired
    BlacklistArticleRepository blacklistArticleRepository

    def "findByObjectId"() {
        when:
        final actual = blacklistArticleRepository.findByObjectId("35186361")

        then:
        actual.isPresent()
    }

}
