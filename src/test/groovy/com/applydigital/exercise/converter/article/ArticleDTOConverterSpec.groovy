package com.applydigital.exercise.converter.article

import com.applydigital.exercise.jpa.entity.article.ArticleEntity
import com.applydigital.exercise.jpa.entity.article.ArticleTagEntity
import spock.lang.Specification

import java.time.Instant

class ArticleDTOConverterSpec extends Specification {

    def articleTagDTOConverter = Mock(ArticleTagDTOConverter)
    def articleDTOConverter = new ArticleDTOConverter(articleTagDTOConverter)

    def "return null when entity is null"() {
        when:
        final actual = articleDTOConverter.apply(null)

        then:
        Objects.isNull(actual)
    }

    def "return DTO"() {
        given:
        final articleTagEntity1 = new ArticleTagEntity.Builder()
                .id(UUID.randomUUID())
                .tag("This is a tag")
                .build()
        articleTagDTOConverter.apply(articleTagEntity1) >> "This is a tag"

        and:
        final articleTagEntity2 = new ArticleTagEntity.Builder()
                .id(UUID.randomUUID())
                .tag("This is another tag")
                .build()
        articleTagDTOConverter.apply(articleTagEntity2) >> "This is another tag"

        and:
        final articleEntity = new ArticleEntity.Builder()
                .id(UUID.randomUUID())
                .createdAt(Instant.now())
                .objectId("1")
                .parentId(2)
                .storyId(3)
                .title("This is a title")
                .url("https://somewhere.com/here")
                .author("Pedro Gonzalez Gutierrez")
                .points(4)
                .storyText("This is the story text")
                .storyTitle("This is the story title")
                .storyUrl("https://randomsite.com/there")
                .commentText("This is amazing")
                .numComments(5)
                .tags([articleTagEntity1, articleTagEntity2])
                .build()

        when:
        final actual = articleDTOConverter.apply(articleEntity)

        then:
        with(actual) {
            id == articleEntity.getId()
            createdAt == articleEntity.getCreatedAt().toString()
            objectId == "1"
            parentId == 2
            storyId == 3
            title == "This is a title"
            url == "https://somewhere.com/here"
            author == "Pedro Gonzalez Gutierrez"
            points == 4
            storyText == "This is the story text"
            storyTitle == "This is the story title"
            storyUrl == "https://randomsite.com/there"
            commentText == "This is amazing"
            numComments == 5
            tags == ["This is a tag", "This is another tag"]
        }
    }

}
