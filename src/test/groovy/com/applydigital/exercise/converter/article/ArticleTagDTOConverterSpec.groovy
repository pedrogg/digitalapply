package com.applydigital.exercise.converter.article

import com.applydigital.exercise.jpa.entity.article.ArticleTagEntity
import spock.lang.Specification

class ArticleTagDTOConverterSpec extends Specification {

    def articleTagDTOConverter = new ArticleTagDTOConverter()

    def "return null when entity is null"() {
        when:
        final actual = articleTagDTOConverter.apply(null)

        then:
        Objects.isNull(actual)
    }

    def "return DTO"() {
        given:
        final entity = new ArticleTagEntity.Builder()
                .id(UUID.randomUUID())
                .tag("This is a tag")
                .build()

        when:
        final actual = articleTagDTOConverter.apply(entity)

        then:
        Objects.equals(actual, "This is a tag")
    }

}
