package com.applydigital.exercise.converter.algolia

import com.applydigital.exercise.api.algolia.dto.AlgoliaPageHit
import spock.lang.Specification

import java.time.Instant

class AlgoliaPageHitConverterSpec extends Specification {

    def algoliaPageHitConverter = new AlgoliaPageHitConverter()

    def "return null when entity is null"() {
        when:
        final actual = algoliaPageHitConverter.apply(null)

        then:
        Objects.isNull(actual)
    }

    def "return entity"() {
        given:
        final entity = new AlgoliaPageHit.Builder()
                .created_at("2022-05-28T17:15:20.196961Z")
                .created_at_i(1L)
                .title("This is a title")
                .url("https://somewhere.com/here")
                .author("Pedro Gonzalez Gutierrez")
                .points(2)
                .story_text("This is the story text")
                .comment_text("This is amazing")
                .num_comments(3)
                .story_id(4)
                .story_title("This is the story title")
                .story_url("https://somewhere.com/there")
                .parent_id(5)
                ._tags(["tag1", "tag2"])
                .objectID("6")
                .build()

        when:
        final actual = algoliaPageHitConverter.apply(entity)

        then:
        with(actual) {
            createdAt == Instant.parse("2022-05-28T17:15:20.196961Z")
            objectId == "6"
            parentId == 5
            storyId == 4
            title == "This is a title"
            url == "https://somewhere.com/here"
            author == "Pedro Gonzalez Gutierrez"
            points == 2
            storyText == "This is the story text"
            storyTitle == "This is the story title"
            storyUrl == "https://somewhere.com/there"
            commentText == "This is amazing"
            numComments == 3
        }
    }

}
