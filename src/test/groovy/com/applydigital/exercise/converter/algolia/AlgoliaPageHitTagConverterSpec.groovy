package com.applydigital.exercise.converter.algolia

import spock.lang.Specification

class AlgoliaPageHitTagConverterSpec extends Specification {

    def algoliaPageHitTagConverter = new AlgoliaPageHitTagConverter()

    def "return null when entity is null"() {
        when:
        final actual = algoliaPageHitTagConverter.apply(null)

        then:
        Objects.isNull(actual)
    }

    def "return DTO"() {
        when:
        final actual = algoliaPageHitTagConverter.apply("This is a tag")

        then:
        with(actual) {
            tag == "This is a tag"
        }
    }

}
