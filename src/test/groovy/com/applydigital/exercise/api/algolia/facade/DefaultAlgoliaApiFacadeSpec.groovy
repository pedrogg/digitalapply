package com.applydigital.exercise.api.algolia.facade

import com.applydigital.exercise.api.algolia.client.AlgoliaApiClient
import com.applydigital.exercise.api.algolia.dto.AlgoliaPage
import okhttp3.Headers
import org.springframework.http.HttpHeaders
import retrofit2.Response
import retrofit2.mock.Calls
import spock.lang.Specification

class DefaultAlgoliaApiFacadeSpec extends Specification {

    def algoliaApiClient = Mock(AlgoliaApiClient)
    def defaultAlgoliaApiFacade = new DefaultAlgoliaApiFacade(algoliaApiClient)

    def "findArticles: exception in the HTTP request"() {
        given:
        algoliaApiClient.findArticles("java", 0) >> { throw new RuntimeException("This is not working") }

        when:
        final actual = defaultAlgoliaApiFacade.findArticles("java", 0)

        then:
        actual.isEmpty()
    }

    def "findArticles: return a page"() {
        given:
        algoliaApiClient.findArticles("java", 0) >> Calls.response(Response.success(200, new AlgoliaPage()))

        when:
        final actual = defaultAlgoliaApiFacade.findArticles("java", 0)

        then:
        actual.isPresent()
    }

}
