package com.applydigital.exercise.service.article

import com.applydigital.exercise.controller.error.ApiErrorMessage
import com.applydigital.exercise.converter.article.ArticleDTOConverter
import com.applydigital.exercise.dto.entity.article.ArticleDTO
import com.applydigital.exercise.dto.search.FindArticlesDTO
import com.applydigital.exercise.dto.search.Months
import com.applydigital.exercise.exception.ApiValidationException
import com.applydigital.exercise.jpa.entity.article.ArticleEntity
import com.applydigital.exercise.jpa.entity.blacklist.BlacklistArticleEntity
import com.applydigital.exercise.jpa.repository.article.ArticleRepository
import com.applydigital.exercise.jpa.repository.blacklist.BlacklistArticleRepository
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import spock.lang.Specification

class DefaultArticleServiceSpec extends Specification {

    def articleRepository = Mock(ArticleRepository)
    def blacklistArticleRepository = Mock(BlacklistArticleRepository)
    def articleDTOConverter = Mock(ArticleDTOConverter)
    def defaultArticleService = new DefaultArticleService(articleRepository, blacklistArticleRepository, articleDTOConverter)

    def objectId = "12345"

    def "findArticles"() {
        given:
        final findArticlesDTO = new FindArticlesDTO.Builder()
                .author("Pedro Gonzalez Gutierrez")
                .title("ApplyDigital exercise")
                .tag("tag1")
                .month(Months.FEBRUARY)
                .build()

        and:
        final page = PageRequest.of(0, 20)

        and:
        final articleEntity1 = new ArticleEntity.Builder()
                .id(UUID.randomUUID())
                .build()
        final articleDTO1 = new ArticleDTO.Builder()
                .id(articleEntity1.getId())
                .build()
        articleDTOConverter.apply(articleEntity1) >> articleDTO1

        and:
        final articleEntity2 = new ArticleEntity.Builder()
                .id(UUID.randomUUID())
                .build()
        final articleDTO2 = new ArticleDTO.Builder()
                .id(articleEntity2.getId())
                .build()
        articleDTOConverter.apply(articleEntity2) >> articleDTO2

        and:
        articleRepository.findAll(_ as org.springframework.data.jpa.domain.Specification, _ as Pageable) >> new PageImpl<ArticleEntity>([articleEntity1, articleEntity2])
        articleRepository.findByIds([articleEntity1.getId(), articleEntity2.getId()], _ as Sort) >> [articleEntity1, articleEntity2]

        when:
        final actual = defaultArticleService.findArticles(findArticlesDTO, page)

        then:
        actual.getContent() == [articleDTO1, articleDTO2]
    }

    def "blacklist: fails when article has been already added"() {
        given:
        final blacklistArticleEntity = new BlacklistArticleEntity.Builder()
                .id(UUID.randomUUID())
                .objectId(objectId)
                .build()
        blacklistArticleRepository.findByObjectId(objectId) >> Optional.of(blacklistArticleEntity)

        when:
        defaultArticleService.blacklist(objectId)

        then:
        final ex = thrown ApiValidationException
        ex.getCode() == ApiErrorMessage.ARTICLE_ALREADY_BLACKLISTED.getCode()
    }

    def "blacklist: fails when article to blacklist not found"() {
        given:
        blacklistArticleRepository.findByObjectId(objectId) >> Optional.empty()

        and:
        articleRepository.findByObjectId(objectId) >> Optional.empty()

        when:
        defaultArticleService.blacklist(objectId)

        then:
        final ex = thrown ApiValidationException
        ex.getCode() == ApiErrorMessage.ARTICLE_NOT_FOUND.getCode()
    }

    def "blacklist: fails when persisting BlacklistArticle"() {
        given:
        blacklistArticleRepository.findByObjectId(objectId) >> Optional.empty()

        and:
        final articleEntity = new ArticleEntity.Builder()
                .id(UUID.randomUUID())
                .objectId(objectId)
                .build()
        articleRepository.findByObjectId(objectId) >> Optional.of(articleEntity)

        and:
        blacklistArticleRepository.save(_ as BlacklistArticleEntity) >> { throw new RuntimeException("Error persisting in DB") }

        when:
        defaultArticleService.blacklist(objectId)

        then:
        final ex = thrown ApiValidationException
        ex.getCode() == ApiErrorMessage.UNABLE_TO_PERSIST_ELEMENT.getCode()
    }

    def "blacklist: article deleted and blacklisted successfully"() {
        given:
        blacklistArticleRepository.findByObjectId(objectId) >> Optional.empty()

        and:
        final articleEntity = new ArticleEntity.Builder()
                .id(UUID.randomUUID())
                .objectId(objectId)
                .build()
        articleRepository.findByObjectId(objectId) >> Optional.of(articleEntity)

        when:
        defaultArticleService.blacklist(objectId)

        then:
        1 * articleRepository.delete(articleEntity)
        1 * blacklistArticleRepository.save(_ as BlacklistArticleEntity)
    }

}
