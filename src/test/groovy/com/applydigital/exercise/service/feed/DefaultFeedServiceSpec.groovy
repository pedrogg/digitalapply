package com.applydigital.exercise.service.feed

import com.applydigital.exercise.api.algolia.dto.AlgoliaPage
import com.applydigital.exercise.api.algolia.dto.AlgoliaPageHit
import com.applydigital.exercise.api.algolia.facade.AlgoliaApiFacade
import com.applydigital.exercise.converter.algolia.AlgoliaPageHitConverter
import com.applydigital.exercise.converter.algolia.AlgoliaPageHitTagConverter
import com.applydigital.exercise.jpa.entity.article.ArticleEntity
import com.applydigital.exercise.jpa.entity.blacklist.BlacklistArticleEntity
import com.applydigital.exercise.jpa.repository.article.ArticleRepository
import com.applydigital.exercise.jpa.repository.blacklist.BlacklistArticleRepository
import spock.lang.Specification

class DefaultFeedServiceSpec extends Specification {

    def algoliaApiFacade = Mock(AlgoliaApiFacade)
    def articleRepository = Mock(ArticleRepository)
    def blacklistArticleRepository = Mock(BlacklistArticleRepository)
    def algoliaPageHitConverter = Mock(AlgoliaPageHitConverter)
    def algoliaPageHitTagConverter = Mock(AlgoliaPageHitTagConverter)
    def defaultFeedService = new DefaultFeedService(
            algoliaApiFacade,
            articleRepository,
            blacklistArticleRepository,
            algoliaPageHitConverter,
            algoliaPageHitTagConverter)

    final blacklistObjectId = "1"
    final objectId = "2"

    def "two articles retrieved from API, first article is blacklisted so is not persisted again in DB, second article is persisted successfully in DB"() {
        given:
        final algoliaArticle1 = new AlgoliaPageHit.Builder()
                .objectID(blacklistObjectId)
                .build()
        final algoliaArticle2 = new AlgoliaPageHit.Builder()
                .objectID(objectId)
                .build()
        final algoliaPage = new AlgoliaPage.Builder()
                .page(0)
                .nbPages(0)
                .hits([algoliaArticle1, algoliaArticle2])
                .build()
        algoliaApiFacade.findArticles("java", 0) >> Optional.of(algoliaPage)

        and:
        final blacklistEntity = new BlacklistArticleEntity.Builder()
                .id(UUID.randomUUID())
                .objectId(blacklistObjectId)
                .build()
        blacklistArticleRepository.findByObjectId(blacklistObjectId) >> Optional.of(blacklistEntity)
        blacklistArticleRepository.findByObjectId(objectId) >> Optional.empty()

        and:
        final articleEntity2 = new ArticleEntity.Builder()
                .id(UUID.randomUUID())
                .objectId(objectId)
                .build()
        algoliaPageHitConverter.apply(algoliaArticle2) >> articleEntity2

        when:
        defaultFeedService.populateArticles()

        then:
        2 * articleRepository.save(articleEntity2) >> articleEntity2
    }

}
