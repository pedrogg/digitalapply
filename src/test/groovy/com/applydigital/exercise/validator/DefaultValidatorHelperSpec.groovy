package com.applydigital.exercise.validator

import org.springframework.mock.web.MockMultipartFile
import org.springframework.validation.BeanPropertyBindingResult
import spock.lang.Specification

class DefaultValidatorHelperSpec extends Specification {

    def validatorHelper = new DefaultValidatorHelper()

    /*
     * rejectIfStringIsBlank
     */

    def "rejectIfStringIsBlank: will reject the string '#string' if null, empty or whitespaces"() {
        given:
        final validationObject = new ValidationObject()
        validationObject.setStringField(string)
        final errors = new BeanPropertyBindingResult(validationObject, "validationObject")

        when:
        validatorHelper.rejectIfStringIsBlank("stringField", string, errors)

        then:
        errors.getErrorCount() == 1

        where:
        string || _
        null   || _
        ""     || _
        "   "  || _
    }

    def "rejectIfStringIsBlank: will not reject a String when is not null, empty or whitespaces"() {
        given:
        final validationObject = new ValidationObject()
        validationObject.setStringField(string)
        final errors = new BeanPropertyBindingResult(validationObject, "validationObject")

        when:
        validatorHelper.rejectIfStringIsBlank("stringField", string, errors)

        then:
        !errors.hasErrors()

        where:
        string                                                                                               || _
        "0123456789 ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz !\"#\$%&'()*+,-./:;<=>?[\\^_{|}~@" || _
    }

    /*
     * rejectStringIfNotInLength
     */

    def "rejectStringIfNotInLength: will reject the string '#string' if length not between #minLength and #maxLength"() {
        given:
        final validationObject = new ValidationObject()
        validationObject.setStringField(string)
        final errors = new BeanPropertyBindingResult(validationObject, "validationObject")

        when:
        validatorHelper.rejectStringIfNotInLength("stringField", string, minLength, maxLength, errors)

        then:
        errors.hasErrors()

        where:
        string                     | minLength | maxLength | _
        null                       | 1         | 3         | _
        ""                         | 1         | 3         | _
        "       "                  | 1         | 3         | _
        "Pedro"                    | 6         | 10        | _
        "Pedro Gonzalez Gutierrez" | 6         | 10        | _
    }

    def "rejectStringIfNotInLength: will not reject the string '#string' if length between #minLength and #maxLength"() {
        given:
        final validationObject = new ValidationObject()
        validationObject.setStringField(string)
        final errors = new BeanPropertyBindingResult(validationObject, "validationObject")

        when:
        validatorHelper.rejectStringIfNotInLength("stringField", string, minLength, maxLength, errors)

        then:
        !errors.hasErrors()

        where:
        string  | minLength | maxLength || _
        "A"     | 1         | 3         || _
        "Pedro" | 3         | 15        || _
        "AAA"   | 1         | 3         || _
    }

    private static class ValidationObject {
        private String stringField;
        private Integer integerField;
        private Long longField;
        private Boolean booleanField;
        private MockMultipartFile multipartFileField;

        String getStringField() {
            return stringField
        }

        void setStringField(String stringField) {
            this.stringField = stringField
        }

        Integer getIntegerField() {
            return integerField
        }

        void setIntegerField(Integer integerField) {
            this.integerField = integerField
        }

        Long getLongField() {
            return longField
        }

        void setLongField(Long longField) {
            this.longField = longField
        }

        Boolean getBooleanField() {
            return booleanField
        }

        void setBooleanField(Boolean booleanField) {
            this.booleanField = booleanField
        }

        MockMultipartFile getMultipartFileField() {
            return multipartFileField
        }

        void setMultipartFileField(MockMultipartFile multipartFileField) {
            this.multipartFileField = multipartFileField
        }

    }
}
