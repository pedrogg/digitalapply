package com.applydigital.exercise.validator.blacklist

import com.applydigital.exercise.dto.blacklist.BlacklistArticleDTO
import com.applydigital.exercise.validator.DefaultValidatorHelper
import org.springframework.core.env.Environment
import org.springframework.validation.BeanPropertyBindingResult
import spock.lang.Specification

class BlacklistArticleDTOValidatorSpec extends Specification {

    def validatorHelper = new DefaultValidatorHelper()
    def environment = Mock(Environment)
    def blacklistArticleDTOValidator = new BlacklistArticleDTOValidator(validatorHelper, environment)

    def setup() throws Exception {
        environment.getProperty("objectId.min") >> 5
        environment.getProperty("objectId.max") >> 10
    }

    def "validation successful"() {
        given:
        final blacklistArticleDTO = new BlacklistArticleDTO.Builder()
                .objectId("35186504")
                .build()
        final errors = new BeanPropertyBindingResult(blacklistArticleDTO, "blacklistArticleDTO")

        when:
        blacklistArticleDTOValidator.validate(blacklistArticleDTO, errors)

        then:
        !errors.hasErrors()
    }

    def "validation fails when objectId is null, empty or whitespaces"() {
        given:
        final blacklistArticleDTO = new BlacklistArticleDTO.Builder()
                .objectId(objectId)
                .build()
        final errors = new BeanPropertyBindingResult(blacklistArticleDTO, "blacklistArticleDTO")

        when:
        blacklistArticleDTOValidator.validate(blacklistArticleDTO, errors)

        then:
        errors.getErrorCount() == 1

        where:
        objectId || _
        null     || _
        ""       || _
        "    "   || _
    }

    def "validation fails when objectId too short"() {
        given:
        final blacklistArticleDTO = new BlacklistArticleDTO.Builder()
                .objectId("3")
                .build()
        final errors = new BeanPropertyBindingResult(blacklistArticleDTO, "blacklistArticleDTO")

        when:
        blacklistArticleDTOValidator.validate(blacklistArticleDTO, errors)

        then:
        errors.getErrorCount() == 1
    }

    def "validation fails when objectId too big"() {
        given:
        final blacklistArticleDTO = new BlacklistArticleDTO.Builder()
                .objectId("351865043518650435186504351865043518650435186504351865043518650435186504")
                .build()
        final errors = new BeanPropertyBindingResult(blacklistArticleDTO, "blacklistArticleDTO")

        when:
        blacklistArticleDTOValidator.validate(blacklistArticleDTO, errors)

        then:
        errors.getErrorCount() == 1
    }

}
