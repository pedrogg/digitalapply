package com.applydigital.exercise.validator.search

import com.applydigital.exercise.dto.search.FindArticlesDTO
import com.applydigital.exercise.validator.DefaultValidatorHelper
import org.springframework.core.env.Environment
import org.springframework.validation.BeanPropertyBindingResult
import spock.lang.Specification

class FindArticlesDTOValidatorSpec extends Specification {

    def validatorHelper = new DefaultValidatorHelper()
    def environment = Mock(Environment)
    def findArticlesDTOValidator = new FindArticlesDTOValidator(validatorHelper, environment)

    def setup() throws Exception {
        environment.getProperty("author.min") >> 2
        environment.getProperty("author.max") >> 30
        environment.getProperty("title.min") >> 2
        environment.getProperty("title.max") >> 30
        environment.getProperty("tag.min") >> 2
        environment.getProperty("tag.max") >> 30
    }

    def "validation successful"() {
        given:
        final findArticlesDTO = new FindArticlesDTO.Builder()
                .author("Pedro Gonzalez Gutierrez")
                .title("ApplyDigital")
                .tag("tag")
                .build()
        final errors = new BeanPropertyBindingResult(findArticlesDTO, "findArticlesDTO")

        when:
        findArticlesDTOValidator.validate(findArticlesDTO, errors)

        then:
        !errors.hasErrors()
    }

    def "validation fails when author too short"() {
        given:
        final findArticlesDTO = new FindArticlesDTO.Builder()
                .author("P")
                .title("ApplyDigital")
                .tag("tag")
                .build()
        final errors = new BeanPropertyBindingResult(findArticlesDTO, "findArticlesDTO")

        when:
        findArticlesDTOValidator.validate(findArticlesDTO, errors)

        then:
        errors.getErrorCount() == 1
    }

    def "validation fails when author too big"() {
        given:
        final findArticlesDTO = new FindArticlesDTO.Builder()
                .author("Pedro Gonzalez Gutierrez Pedro Gonzalez Gutierrez Pedro Gonzalez Gutierrez")
                .title("ApplyDigital")
                .tag("tag")
                .build()
        final errors = new BeanPropertyBindingResult(findArticlesDTO, "findArticlesDTO")

        when:
        findArticlesDTOValidator.validate(findArticlesDTO, errors)

        then:
        errors.getErrorCount() == 1
    }

    def "validation fails when title too short"() {
        given:
        final findArticlesDTO = new FindArticlesDTO.Builder()
                .author("Pedro Gonzalez Gutierrez")
                .title("A")
                .tag("tag")
                .build()
        final errors = new BeanPropertyBindingResult(findArticlesDTO, "findArticlesDTO")

        when:
        findArticlesDTOValidator.validate(findArticlesDTO, errors)

        then:
        errors.getErrorCount() == 1
    }

    def "validation fails when title too big"() {
        given:
        final findArticlesDTO = new FindArticlesDTO.Builder()
                .author("Pedro Gonzalez Gutierrez")
                .title("ApplyDigital ApplyDigital ApplyDigital ApplyDigital ApplyDigital ApplyDigital ApplyDigital")
                .tag("tag")
                .build()
        final errors = new BeanPropertyBindingResult(findArticlesDTO, "findArticlesDTO")

        when:
        findArticlesDTOValidator.validate(findArticlesDTO, errors)

        then:
        errors.getErrorCount() == 1
    }

    def "validation fails when tag too short"() {
        given:
        final findArticlesDTO = new FindArticlesDTO.Builder()
                .author("Pedro Gonzalez Gutierrez")
                .title("ApplyDigital")
                .tag("t")
                .build()
        final errors = new BeanPropertyBindingResult(findArticlesDTO, "findArticlesDTO")

        when:
        findArticlesDTOValidator.validate(findArticlesDTO, errors)

        then:
        errors.getErrorCount() == 1
    }

    def "validation fails when tag too big"() {
        given:
        final findArticlesDTO = new FindArticlesDTO.Builder()
                .author("Pedro Gonzalez Gutierrez")
                .title("ApplyDigital")
                .tag("tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag tag")
                .build()
        final errors = new BeanPropertyBindingResult(findArticlesDTO, "findArticlesDTO")

        when:
        findArticlesDTOValidator.validate(findArticlesDTO, errors)

        then:
        errors.getErrorCount() == 1
    }

}
