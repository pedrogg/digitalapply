package com.applydigital.exercise.controller.error

import com.applydigital.exercise.exception.ApiValidationException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.validation.BindException
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.context.request.WebRequest
import spock.lang.Specification

import static org.assertj.core.api.Assertions.assertThat
import static org.assertj.core.api.Assertions.tuple

class ApiExceptionHandlerSpec extends Specification {

    def headers = Mock(HttpHeaders)
    def request = Mock(WebRequest)
    def bindingResult = Mock(BindingResult)
    def apiExceptionHandler = new ApiExceptionHandler()

    def fieldError1 = new FieldError(
            "object1",
            "stringField",
            "SuColega",
            true,
            null,
            ["argument1", "argument2"] as String[],
            "String message")

    def fieldError2 = new FieldError(
            "object2",
            "integerField",
            "25",
            false,
            [] as String[],
            ["argument1", "argument2"] as String[],
            "Integer message")

    def fieldError3 = new FieldError(
            "object3",
            "booleanField",
            "false",
            true,
            ["booleanCode"] as String[],
            ["argument1", "argument2"] as String[],
            "Boolean message")

    def fieldError4 = new FieldError(
            "object4",
            "doubleField",
            "2.2D",
            false,
            ["doubleCode1", "doubleCode2"] as String[],
            ["argument1", "argument2"] as String[],
            "Double message")

    def "will return ApiErrorsView populated when @Valid fails"() {
        given:
        final methodArgumentNotValidException = new MethodArgumentNotValidException(null, bindingResult)

        and:
        bindingResult.getFieldErrors() >> [fieldError1, fieldError2, fieldError3, fieldError4]

        when:
        final actual = apiExceptionHandler.handleMethodArgumentNotValid(methodArgumentNotValidException, headers, HttpStatus.BAD_REQUEST, request)

        then:
        Objects.nonNull(actual)

        and:
        assertThat(((ApiErrorsView) actual.getBody()).fieldErrors)
                .extracting("field", "code", "message")
                .contains(
                        tuple("stringField", ApiErrorMessage.UNKNOWN_CODE.getCode(), "String message"),
                        tuple("integerField", ApiErrorMessage.UNKNOWN_CODE.getCode(), "Integer message"),
                        tuple("booleanField", "booleanCode", "Boolean message"),
                        tuple("doubleField", "doubleCode2", "Double message"))
    }

    def "will return ApiErrorsView populated when BindException fails"() {
        given:
        final bindException = new BindException(bindingResult)

        and:
        bindingResult.getFieldErrors() >> [fieldError1, fieldError2, fieldError3, fieldError4]

        when:
        final actual = apiExceptionHandler.handleBindException(bindException, headers, HttpStatus.BAD_REQUEST, request)

        then:
        Objects.nonNull(actual)

        and:
        assertThat(((ApiErrorsView) actual.getBody()).fieldErrors)
                .extracting("field", "code", "message")
                .contains(
                        tuple("stringField", ApiErrorMessage.UNKNOWN_CODE.getCode(), "String message"),
                        tuple("integerField", ApiErrorMessage.UNKNOWN_CODE.getCode(), "Integer message"),
                        tuple("booleanField", "booleanCode", "Boolean message"),
                        tuple("doubleField", "doubleCode2", "Double message"))
    }

    def "will return ApiErrorsView populated when thrown ApiValidationException"() {
        given:
        final field = "myField"
        final code = "errorCode"
        final message = "This is an Error"

        and:
        final apiValidationException = new ApiValidationException(field, code, message)

        when:
        final actual = apiExceptionHandler.apiValidationException(apiValidationException)

        then:
        Objects.nonNull(actual)

        and:
        assertThat(((ApiErrorsView) actual.getBody()).fieldErrors)
                .extracting("field", "code", "message")
                .contains(
                        tuple(field, code, message))
    }

}
