# Apply Digital API

This RestAPI got a schedule job that stores articles from AlgoliaAPI in a PostgresSQL database. Also, it exposes two endpoints:
1. Query these articles
2. Blacklist articles so they wont be stored in the database anymore 

The variable `algolia.articles.refresh-delay` means the delay to retrieve articles from AlgoliaAPI again. At the moment is set up in `10 minutes`.

## Technologies
- Gradle 7.3.3
- Spring Boot 2.7.9 (the latest version for Java 11)
- JPA 2.2 and Hibernate 5.6.15 with Hikari pool
- Liquibase 4.19.0 to manage database migrations
- HTTP client Retrofit 2.9.0 to retrieve data from AlgoliaAPI
- Unit and integration test by Spock Framework 2.1 (H2 in memory database for Integration Test)
- Auth0 to issue JWT tokens

## Gradle tasks
**1) Run Unit test**
```
./gradlew test
```
**2) Run Integration test**
```
./gradlew integrationTest
```
**3) Run unit tests, integration tests and build jar**
```
./gradlew clean build
```
This will create the jar in ```build/libs/apply-digital-X.Y.Z.jar```

## Docker
This RestAPI is using docker. In order to run it:
```
./gradlew clean build
docker-compose up --build
```

## Pacts

### Query articles: 
```
GET /article?author=MY_AUTHOR&title=MY_TITLE&tag=MY_TAG&month=MY_MONTH&page=MY_PAGE
Header Authorization: Bearer jwtToken
```
All parameters (`author` , `title`, `tag`, `month` and `page`) are optional and can be combined. The engine is using [Spring JPA Specifications](https://www.baeldung.com/spring-jpa-joining-tables).

The valid values for `month` are: `JANUARY`, `FEBRUARY`, `MARCH`, `APRIL`, `MAY`, `JUNE`, `JULY`, `AUGUST`, `SEPTEMBER`, `OCTOBER`, `NOVEMBER` and `DECEMBER`.
> 200: [Page](https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/domain/PageImpl.html) of [ArticleDTO](com.applydigital.exercise.dto.entity.article/ArticleDTO.java)

### Blacklist article 
```
POST /article/blacklist
Header Authorization: Bearer jwtToken
{
    "objectId": "35197795"
}
```
> 400: Validation Error

> 201: Article deleted and blacklisted

### Get token from Auth0
```
curl --location --request POST 'https://macasupe.eu.auth0.com/oauth/token' \
--header 'application: application/x-www-form-urlencoded' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'username=pedrola@macasupe.com' \
--data-urlencode 'password=f1a0af1328709bf976ff8fa89d22a1D2' \
--data-urlencode 'audience=https://www.macasupe.com' \
--data-urlencode 'client_id=QhggjTM2Z7seFZT7spw9CgqievE7Y7OH' \
--data-urlencode 'scope=openid email profile'
```
## Postman Collection
In the directory `postman` there is a file called ```ApplyDigital.postman_collection.json``` with all these request ready to import in [Postman](https://www.getpostman.com/). Also ```ApplyDigital.postman_environment.json``` with the environment variables.
